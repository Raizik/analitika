import os
import arrow
import time
import shutil
import yaml

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import slovar


def init():
    global configs, LOGIN, PASSWORD
    with open("config.yml", 'r', encoding='UTF-8') as stream:
        try:
            configs = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    LOGIN = configs['Auth']['login']
    PASSWORD = configs['Auth']['password']


def etd_eisz(browser):
    browser.get("https://reports.eisz.kz/Account/Login.aspx")
    browser.find_element_by_id("MainContent_rbSystem_RB2_I_D").click()
    browser.find_element_by_id("MainContent_LoginUser_UserName").send_keys(LOGIN)
    browser.find_element_by_id("MainContent_LoginUser_Password").send_keys(PASSWORD)
    browser.find_element_by_id("MainContent_LoginUser_LoginButton_B").click()


def search_report(browser, text_in_rpn):
    wait = WebDriverWait(browser, 500)
    input_box_poisk = wait.until(EC.visibility_of_element_located(
        (By.ID, "MainContent_ASPxSplitter1_search_box")))
    input_box_poisk.clear()
    input_box_poisk.send_keys(text_in_rpn)
    browser.find_element_by_id("MainContent_ASPxSplitter1_search").click()
    # wait.until(EC.visibility_of_element_located(
    #     (By.ID, text_xpath)))
    element = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//*[@class='dxtl dxtl__B0' and text()[contains(.,'{}')]]".format(text_in_rpn))))
    element[0].click()
    # Ждет пока загрузится отчет!
    wait.until(EC.visibility_of_element_located(
        (By.ID, "MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_calBeginDate_I")))


def last_date():
    """
    Возращает начало предыдущего года и конец предыдущего месяца
    """
    utc = arrow.utcnow()
    if arrow.utcnow().month != 1:
        konec = utc.shift(years=-1, days=-arrow.utcnow().day).format("DD.MM.YYYY")
        nachalo = (utc.shift(years=-1, days=-arrow.utcnow().day+1, months=-arrow.utcnow().month+1)).format("DD.MM.YYYY")
    else:
        konec = utc.shift(years=-1, days=-arrow.utcnow().day).format("DD.MM.YYYY")
        nachalo = utc.shift(years=-2, days=-arrow.utcnow().day+1, months=-arrow.utcnow().month+1).format("DD.MM.YYYY")
    return nachalo, konec


def today_date():
    """
    Возращает начало текущего года года и конец предыдущего месяца
    """
    utc = arrow.utcnow()
    if arrow.utcnow().month != 1:
        konec = utc.shift(days=-arrow.utcnow().day).format("DD.MM.YYYY")
        nachalo = (utc.shift(days=-arrow.utcnow().day+1, months=-arrow.utcnow().month+1)).format("DD.MM.YYYY")
    else:
        konec = utc.shift(days=-arrow.utcnow().day).format("DD.MM.YYYY")
        nachalo = utc.shift(days=-arrow.utcnow().day+1, months=-arrow.utcnow().month+1, years=-1).format("DD.MM.YYYY")
    return nachalo, konec


def nachala_perioda(browser, input_date):
    begin_mo = browser.find_element_by_id(
        "MainContent_ASPxSplitter1_viewer_"
        "reportCriteria_fsrCriteria_calBeginDate_I")
    browser.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", begin_mo, "value", input_date)
    time.sleep(.500)
    browser.execute_script("aspxETextChanged('MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_calBeginDate');")


def konec_perioda(browser, input_date):
    begin_mo = browser.find_element_by_id(
        "MainContent_ASPxSplitter1_viewer_"
        "reportCriteria_fsrCriteria_calEndDate_I")
    browser.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", begin_mo, "value", input_date)
    time.sleep(.500)
    browser.execute_script("aspxETextChanged('MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_calEndDate');")


def istochnik_finansy(browser, input_finance):
    begin_mo = browser.find_element_by_id(
        "MainContent_ASPxSplitter1_viewer_"
        "reportCriteria_fsrCriteria_ddlFinanceSource_I")
    browser.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", begin_mo, "value", input_finance)
    time.sleep(.500)
    browser.execute_script("aspxETextChanged('MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_ddlFinanceSource');")


def vid_okazaniya_pomoshi(browser, input_stacionar):
    begin_mo = browser.find_element_by_id(
        "MainContent_ASPxSplitter1_viewer_"
        "reportCriteria_fsrCriteria_ddlPatientHelpType_I")
    browser.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", begin_mo, "value", input_stacionar)
    browser.execute_script("aspxETextChanged('MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_ddlPatientHelpType');")
    time.sleep(2)


def prolechennie_sluchai(browser, input_sluchai):
    wait = WebDriverWait(browser, 500)
    begin_mo = wait.until(EC.element_to_be_clickable((By.ID, "MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_ddlChecked_I")))
    browser.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", begin_mo, "value", input_sluchai)
    time.sleep(.500)
    browser.execute_script("aspxETextChanged('MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_ddlChecked');")


def select_export_format(browser):
    wait = WebDriverWait(browser, 500)
    export_formats = wait.until(EC.visibility_of_element_located((By.ID, "MainContent_ASPxSplitter1_viewer_ctl00_stiReport_SaveTypeList")))
    for option in export_formats.find_elements_by_tag_name('option'):
        if option.text == "Microsoft Excel":
            option.click()
            break
    time.sleep(1)
    browser.find_element_by_id("MainContent_ASPxSplitter1_viewer_ctl00_stiReport_Save").click()


def save_as():
    months = ["", "Январь", "Февраль", "Март", "Апрель",
              "Май", "Июнь", "Июль", "Август", "Сеньтябрь",
              "Октябрь", "Ноябрь", "Декабрь"]
    creat_file_path = "C:\\РПН\\Аналитика\\" + arrow.utcnow().format("YYYY") + "\\" + months[arrow.now().month-1] + "\\" + arrow.utcnow().format("DD.MM.YYYY") + "\\"
    if not os.path.exists(creat_file_path):
        os.makedirs(creat_file_path)
    return creat_file_path


def show_file(SAVE_TO_DIRECTORY):
    files = os.listdir(SAVE_TO_DIRECTORY)
    file_list = []
    for file in files:
        if file.endswith(".xls"):
            file_list.append(file)
    return file_list


def move_file(tables_name):
    file_names = ['2017 Местный', '2018 Республиканский (БП 067, ПП 100) Туберкулез', '2018 Республиканский (БП 067, ПП 100) Трансферты ФСМС на оплату ГОБМП',
                  '2017 Местный (СЕЛО)', '2017 Республиканский (БП 052)', '2018 Все', '2018 Республиканский (БП 067, ПП 100) АПП', '2018 Республиканский (БП 067, ПП 100) Наркология',
                  '2018 Республиканский (БП 067, ПП 100) Психиатрия', '2018 Республиканский (БП 067, ПП 100) онкология', '2018 Республиканский (БП 067, ПП 100) село',
                  '2017 Все'
                  ]
    path_save_as = save_as()
    excel = show_file(path_save_as)
    new_file = path_save_as + tables_name
    if not os.path.exists(new_file):
        os.makedirs(new_file)
    b = 0
    for i in excel:
        if b < 12:
            shutil.move(path_save_as + i, new_file + "\\" + file_names[b] + ".xls")
            b += 1


def main():
    init()
    begin_last, end_last = last_date()
    begin_today, end_today = today_date()
    print('сейчас = ', arrow.utcnow().format("DD.MM.YYYY"))

    options = webdriver.ChromeOptions()
    options.add_argument("--incognito")

    path_save_as = save_as()
    print(path_save_as)

    prefs = {'download.default_directory': path_save_as}
    options.add_experimental_option('prefs', prefs)
    # options.add_argument('headless')
    # options.add_argument('window-size=1200x600')
    browser = webdriver.Chrome(chrome_options=options)

    # driver = webdriver.Chrome(executable_path=r"E:\selenium\chromedriver.exe",
    #                               chrome_options=chrome_options)

    wait = WebDriverWait(browser, 500)
    etd_eisz(browser)
    spisok = configs['Reports']

    for tables in spisok:
        path_proverki = save_as()
        if not os.path.exists(path_proverki + tables['имя_папки']):
            print(tables['имя_таблицы'], tables['вид_оказания_помощи'], tables['имя_папки'])
            search_report(browser, tables['имя_таблицы'])
            time.sleep(1)
            nachala_perioda(browser, begin_last)
            konec_perioda(browser, end_last)
            vid_okazaniya_pomoshi(browser, tables['вид_оказания_помощи'])
            prolechennie_sluchai(browser, 'Подтвержденные случаи')
            for i in slovar.finansy_2017:
                istochnik_finansy(browser, i)
                browser.find_element_by_id("MainContent_ASPxSplitter1_viewer_reportCriteria_buttonShowReport_CD").click()
                select_export_format(browser)
                browser.find_element_by_id("MainContent_ASPxSplitter1_viewer_bBack_CD").click()
                wait.until(EC.visibility_of_element_located(
                    (By.ID, "MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_calBeginDate_I")))
            nachala_perioda(browser, begin_today)
            konec_perioda(browser, end_today)
            vid_okazaniya_pomoshi(browser, tables['вид_оказания_помощи'])
            prolechennie_sluchai(browser, 'Подтвержденные случаи')
            for b in slovar.finansy_2018:
                istochnik_finansy(browser, b)
                browser.find_element_by_id("MainContent_ASPxSplitter1_viewer_reportCriteria_buttonShowReport_CD").click()
                select_export_format(browser)
                browser.find_element_by_id("MainContent_ASPxSplitter1_viewer_bBack_CD").click()
                wait.until(EC.visibility_of_element_located(
                    (By.ID, "MainContent_ASPxSplitter1_viewer_reportCriteria_fsrCriteria_calBeginDate_I")))
            time.sleep(5)
            move_file(tables['имя_папки'])
    browser.quit()


if __name__ == '__main__':
    main()
